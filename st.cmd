require essioc
require xtpico
require iocmetadata

iocshLoad("$(essioc_DIR)/common_config.iocsh")

epicsEnvSet("PREFIX",               "Spk-090RFC:RFS-PIND-210")
epicsEnvSet("DEVICE_IP",            "spk9-rf2-pd.tn.esss.lu.se")

epicsEnvSet("I2C_COMM_PORT",        "AK_I2C_COMM")
epicsEnvSet("R_TMP100",             ":Temp")
epicsEnvSet("R_M24M02",             ":Eeprom")
epicsEnvSet("R_TCA9555",            ":IOExp")
epicsEnvSet("R_LTC2991",            ":VMon")

drvAsynIPPortConfigure($(I2C_COMM_PORT),"$(DEVICE_IP):1002")

# PMT temperature sensor
iocshLoad("$(xtpico_DIR)/tmp100.iocsh", "IP_PORT=$(I2C_COMM_PORT), N=1, NAME=$(R_TMP100), COUNT=1, INFOS=0x49")

# one EEPROM
iocshLoad("$(xtpico_DIR)/m24m02.iocsh", "IP_PORT=$(I2C_COMM_PORT), N=1, NAME=$(R_M24M02), COUNT=1, INFOS=0x50")

# I2C port extender
iocshLoad("$(xtpico_DIR)/tca9555.iocsh", "IP_PORT=$(I2C_COMM_PORT), N=1, NAME=$(R_TCA9555), COUNT=1, INFOS=0x21")

# Voltage and current monitor
iocshLoad("$(xtpico_DIR)/ltc2991.iocsh", "IP_PORT=$(I2C_COMM_PORT), N=1, NAME=$(R_LTC2991), COUNT=1, INFOS=0x48")

dbLoadRecords("$(E3_CMD_TOP)/pin-diode.db", "P=$(PREFIX), R_TMP100=$(R_TMP100), R_LTC2991=$(R_LTC2991), R_TCA9555=$(R_TCA9555)")

## Port Expander Initial config
afterInit("dbpf $(PREFIX)$(R_TCA9555)-DirTestLED1 0")
afterInit("dbpf $(PREFIX)$(R_TCA9555)-DirTestLED2 0")
afterInit("dbpf $(PREFIX)$(R_TCA9555)-TestLED1 0")
afterInit("dbpf $(PREFIX)$(R_TCA9555)-TestLED2 0")

pvlistFromInfo("ARCHIVE_THIS", "$(IOCNAME):ArchiverList")

iocInit()

